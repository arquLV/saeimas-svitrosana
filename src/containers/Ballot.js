import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Ballot from '../components/Ballot';

import * as partyActions from '../actions/parties';

const mapStateToProps = state => ({
    parties: state.parties,
});

const mapDispatchToProps = dispatch => ({
    ...bindActionCreators(partyActions, dispatch),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Ballot);