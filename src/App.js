import React, { Component } from 'react';
import globalStyling from './globalStyling';

import Calculator from './views/Calculator';

class App extends Component {
    render() {
        globalStyling();
        return (
            <div className="App">
                <Calculator />
            </div>
        );
    }
}

export default App;
