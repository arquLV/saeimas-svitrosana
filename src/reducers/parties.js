const MOCK_PARTY = {
    title: 'Kartupeļi LV',
    number: 15,
    region: 'Rīga',
    candidates: [
        {
            name: 'Artuss Viens',
            criteria: {
                higherEducation: false,
                isArtuss: true,
                communist: false,
                previousExperience: false,
            },
            status: 1,
        },
        {
            name: 'Aldis Divi',
            criteria: {
                higherEducation: true,
                isArtuss: false,
                communist: false,
                previousExperience: false,
            },
            status: 0,
        },
        {
            name: 'Nejaušs Kivičs',
            criteria: {
                higherEducation: false,
                isArtuss: false,
                communist: false,
                previousExperience: false,
            },
            status: -1,
        }
    ]
};

const initialPartiesState = {
    list: new Array(16),
    selected: 0,
};

// temp
initialPartiesState.list[MOCK_PARTY.number - 1] = MOCK_PARTY;
initialPartiesState.selected = MOCK_PARTY.number - 1;

export default (state = initialPartiesState, action) => {
    switch (action.type) {
        case 'SET_PARTY_DATA':
            const { number } = action.data;
            
            const { list } = state;
            list[number - 1] = action.data;

            return { ...state, list };
        default:
            return state;
    }
}