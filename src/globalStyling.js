import { injectGlobal } from 'styled-components';
import reset from 'styled-reset';

const globalStyling = () => injectGlobal`
    ${reset};

    html, body {
        background: #eee;
        font-family: sans-serif;
        font-size: 10px;
    }
`;

export default globalStyling;
