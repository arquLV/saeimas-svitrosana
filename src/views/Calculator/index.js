import React from 'react';

import CriteriaMenu from '../../components/CriteriaMenu';

import Ballot from '../../containers/Ballot';

import {
    CalculatorContainer,
    BallotContainer,
    CriteriaContainer,
} from './styles';

class Calculator extends React.PureComponent {

    render() {
        return (
            <CalculatorContainer>
                <BallotContainer>
                    <Ballot />
                </BallotContainer>
                <CriteriaContainer>
                    <CriteriaMenu />
                </CriteriaContainer>
            </CalculatorContainer>
        );
    }
}

export default Calculator;