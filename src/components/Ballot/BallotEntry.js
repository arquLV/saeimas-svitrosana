import React from 'react';

import {
    BallotEntryContainer,
    CandidateNumber,
    CandidateName,
    CandidateTickBox,
    CandidateDataContainer,
} from './styles';

class BallotEntry extends React.PureComponent {
    render() {
        return (
            <BallotEntryContainer>
                <CandidateDataContainer crossedOut={this.props.status < 0}>
                    <CandidateNumber>{this.props.number}.</CandidateNumber>
                    <CandidateName>{this.props.children}</CandidateName>
                </CandidateDataContainer>
                <CandidateTickBox ticked={this.props.status > 0} />
            </BallotEntryContainer>
        );
    }
}

export default BallotEntry;