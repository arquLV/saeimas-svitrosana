import React from 'react';

import BallotEntry from './BallotEntry';

import {
    BallotContainer,
    HeadingContainer,
    ElectionLabel,
    RegionLabel,
    BallotTitle,
    BallotNumber,
    BallotList,
} from './styles';

class Ballot extends React.Component {

    render() {
        const currentParty = this.props.parties.list[this.props.parties.selected];
        const { title, number, region, candidates } = currentParty;

        return (
            <BallotContainer>
                <HeadingContainer>
                    <ElectionLabel>13. Saeimas vēlēšanas</ElectionLabel>
                    <RegionLabel>{region}</RegionLabel>
                </HeadingContainer>

                <HeadingContainer>
                    <BallotTitle>{title}</BallotTitle>
                    <BallotNumber>{number}</BallotNumber>
                </HeadingContainer>

                <BallotList>
                    {candidates.map((candidate, idx) => (
                        <BallotEntry
                            key={`${number}-${idx}`}
                            number={idx+1}
                            status={candidate.status}
                        >{candidate.name}</BallotEntry>
                    ))}
                </BallotList>
            </BallotContainer>
        );
    }
}

export default Ballot;