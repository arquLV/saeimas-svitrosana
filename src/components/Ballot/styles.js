import styled, { css } from 'styled-components';
import { FlexContainerJustified } from '../baseComponents';

export const BallotContainer = styled.div`
    background: #fff;
    box-sizing: border-box;
    width: 540px;
    height: 720px;
    padding: 20px 35px;
    box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.1);
`;

export const BallotList = styled.ul`
    position: relative;
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    padding-right: 8px;
`;

export const HeadingContainer = styled(FlexContainerJustified)`
    margin-bottom: 30px;
`;

const BallotInfoLabel = styled.h4`
    color: #aaa;
    font-weight: bold;
    font-size: 1.4rem;
`;
export const ElectionLabel = styled(BallotInfoLabel)`

`;
export const RegionLabel = styled(BallotInfoLabel)`

`;

const BallotHeading = styled.h2`
    font-weight: bold;
    font-size: 2rem;
`;
export const BallotTitle = styled(BallotHeading)`

`;
export const BallotNumber = styled(BallotHeading)`

`;

export const BallotEntryContainer = styled.li`
    position: relative;
    display: block;
    width: 46%;
    height: 20px;
    border-bottom: 1px solid #aaa;
    font-size: 1.8rem;
    margin: 12px 0;
`;

export const CandidateNumber = styled.span`
    margin-right: 10px;
`;

export const CandidateName = styled.span`
`;

export const CandidateTickBox = styled.div`
    position: relative;
    display: block;
    float: right;

    border:1px solid #aaa;
    width: 15px;
    height: 15px;
    transform: translateX(50%) rotate(45deg);

    ${props => props.ticked && css`
        &::before, &::after {
            content: '';
            position: absolute;
            display: block;
            height: 2px;
            width: 21px;
            background: #000;
            top: 50%;
            left: 50%;
        }
        
        &::before {
            transform: translate(-50%, -50%) rotate(-45deg);
        }
        &::after {
            transform: translate(-50%, -50%) rotate(45deg);
        }
    `};
`;

export const CandidateDataContainer = styled.div`
    position: relative;
    display: block;
    float: left;

    ${props => props.crossedOut && css`
        &::before {
            content: '';
            position: absolute;
            display: block;
            height: 2px;
            width: calc(100% + 10px);
            background: #000;
            left: -5px;
            top: 50%;
            transform: translateY(-50%);
        }
    `};
`;
