import React from 'react';

import {
    CriteriaContainer,
    CriteriaTitle,
    CriteriaList,
    CriterionTag,
} from './styles';

class CriteriaMenu extends React.PureComponent {
    render() {
        return (
            <CriteriaContainer>
                <CriteriaTitle>Atzīmē kritērijus, kuri, tavuprāt, atbilst pienācīgam Saeimas deputāta kandidātam:</CriteriaTitle>
                <CriteriaList>
                    <CriterionTag>ir augstākā izglītība</CriterionTag>
                    <CriterionTag>ir Artuss</CriterionTag>
                    <CriterionTag>nav bijis Latvijas Komunistiskajā partijā</CriterionTag>
                    <CriterionTag>iepriekš jau strādājis Saeimā</CriterionTag>
                </CriteriaList>
            </CriteriaContainer>
        );
    }
}

export default CriteriaMenu;