import styled, { css } from 'styled-components';

export const CriteriaContainer = styled.div`
    height: 100%;
    padding: 50px 30px;
    background: #fff;
    max-width: 500px;
`;

export const CriteriaTitle = styled.h2`
    font-size: 2rem;
    margin-bottom: 30px;
`;

export const CriteriaList = styled.div`
    display: flex;
    flex-wrap: wrap;
`;

export const CriterionTag = styled.a`
    display: block;
    font-size: 1.4rem;
    padding: 10px 15px;
    margin: 0 15px 15px 0;
    border-radius: 5px;
    cursor: pointer;
    background: #eee;

    &:hover {
        background: #ccc;
    }

    ${props => props.active && css`
        background: #888;
    `};
`;