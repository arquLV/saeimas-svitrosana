import styled from 'styled-components';

export const FlexContainerJustified = styled.div`
    display: flex;
    justify-content: space-between;
`;