const MOCK_PARTY = {
    title: 'Kartupeļi LV',
    number: 15,
    region: 'Rīga',
    candidates: [
        {
            name: 'Artuss Viens',
            criteria: {
                higherEducation: false,
                isArtuss: true,
                communist: false,
                previousExperience: false,
            },
        },
        {
            name: 'Aldis Divi',
            criteria: {
                higherEducation: true,
                isArtuss: false,
                communist: false,
                previousExperience: false,
            },
        },
        {
            name: 'Nejaušs Kivičs',
            criteria: {
                higherEducation: false,
                isArtuss: false,
                communist: false,
                previousExperience: false,
            },
        }
    ]
};

export const fetchParty = id => dispatch => {
    // getPartyDataFromSomewhere(id)
    const partyData = MOCK_PARTY;

    partyData.candidates = partyData.candidates.map(candidate => {
        candidate.status = 0;
        return candidate;
    });

    dispatch({
        type: 'SET_PARTY_DATA',
        data: partyData,
    });
};